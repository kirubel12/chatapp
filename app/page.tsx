import Image from 'next/image'
import {Button} from "@/components/ui/button";

export default function Home() {
  return (
      <main>
        <h1>Homepage</h1>
        <Button>Hello world</Button>
      </main>
  )
}
